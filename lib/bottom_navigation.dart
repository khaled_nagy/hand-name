
import 'package:flutter/material.dart';
import 'package:hand_made/src/screens/family_producer/home_screen.dart';
import 'package:hand_made/src/screens/family_producer/notification_screen.dart';
import 'package:hand_made/src/screens/family_producer/talks_screen.dart';
import 'package:hand_made/src/screens/family_producer/login1_screen.dart';


class BottomNavigation extends StatefulWidget {


  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<BottomNavigation> {

 int countCart;








  @override
  void initState() {
    _pageController = new PageController();
  }



  GlobalKey<ScaffoldState> _globalKeyScafoldState =
      new GlobalKey<ScaffoldState>();
  TextEditingController _searchController = new TextEditingController();

  var index = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int _page = 0;
  PageController _pageController;

  navigationTapped(int page) {
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 100), curve: Curves.ease);
  }

  void onPageChanged(int page) {
    print(page);
    setState(() {
      this._page = page;
    });
  }

  title() {
    if (_page == 1)
      return "الاعلانات التجاريه";
    else if (_page == 2)
      return "رفع اعلان";
    else if (_page == 3) return "الملف الشخصي";
  }




  @override
  Widget build(BuildContext context) {
    BottomNavigationBarItem Home = new BottomNavigationBarItem(
        title: new Container(),
        icon: new Column(
          children: <Widget>[
            new Container(
                width: 24.0,
                height: 24.0,
                child: new Image.asset(_page == 0
                    ? "assets/imgs/ic_tab_bar_home_active.png"
                    : "assets/imgs/ic_tab_bar_home.png")),
            new Container(height: 4.0),
            new Text("الرئيسية",
                style: new TextStyle(
                  color: Colors.grey[500],
                  fontSize: 12.0,
                ))
          ],
        ));

    BottomNavigationBarItem Notification = new BottomNavigationBarItem(
        title: new Container(),
        icon: new Column(
          children: <Widget>[
            new Container(
                width: 24.0,
                height: 24.0,
                child: new Image.asset(_page == 1
                    ? "assets/imgs/ic_tab_bar_notification_active.png"
                    : "assets/imgs/ic_tab_bar_notification.png")),
            new Container(height: 4.0),
            new Text("الاشعارات",
                style: new TextStyle(
                  color: Colors.grey[500],
                  fontSize: 12.0,
                ))
          ],
        ));

    BottomNavigationBarItem Chat = new BottomNavigationBarItem(
        title: new Container(),
        icon: new Column(
          children: <Widget>[
            new Container(
                width: 24.0,
                height: 24.0,
                child: new Image.asset(_page == 2
                    ? "assets/imgs/ic_tab_bar_chat_active.png"
                    : "assets/imgs/ic_tab_bar_chat.png")),
            new Container(height: 4.0),
            new Text("المحادثة",
                style: new TextStyle(
                  color: Colors.grey[500],
                  fontSize: 12.0,
                ))
          ],
        ));

    BottomNavigationBarItem MyData = new BottomNavigationBarItem(
        title: new Container(),
        icon: new Column(
          children: <Widget>[
            new Container(
                width: 24.0,
                height: 24.0,
                child: new Image.asset(_page == 3
                    ? "assets/imgs/ic_tab_bar_profile_active.png"
                    : "assets/imgs/ic_tab_bar_profile.png")),
            new Container(height: 4.0),
            new Text("بياناتى ",
                style: new TextStyle(
                  color: Colors.grey[500],
                  fontSize: 12.0,
                ))
          ],
        ));

 

    Widget btns = new BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          Home,
          Notification,
          Chat,
          MyData,
          
        ],
        onTap: navigationTapped,
        currentIndex: _page);

    return new Scaffold(
     
      key: _globalKeyScafoldState,
      bottomNavigationBar: btns,
      
      body: new PageView(
          scrollDirection: Axis.horizontal,
          pageSnapping: true,
          reverse: true,
          physics: const NeverScrollableScrollPhysics(),
          children: [
              HomeScreen(),
            NotificationScreen(),
            TalksScreen(),
            Login1Screen(),
            
          ],
          controller: _pageController,
          onPageChanged: onPageChanged),
      /**/
    );
  }
}
