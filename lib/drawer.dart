import 'package:flutter/material.dart';

class DrawerW {
  Widget showDrawerUser(
    BuildContext context,
  ) {
    return new Container(
      width: MediaQuery.of(context).size.width /1.3,
      child: new Drawer(
        child: new Container(
          decoration: BoxDecoration(color: const Color(0xff8C0343)),
          child: new ListView(
            children: <Widget>[
              new DrawerHeader(
                  decoration: BoxDecoration(
                    color: const Color(0xffBF1162),
                  ),
                  child: new Column(
                    children: <Widget>[
                      Directionality(
                        textDirection: TextDirection.ltr,
                        child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 8, 0),
                                child: CircleAvatar(
                                  radius: 12,
                                  backgroundImage: AssetImage(
                                      "assets/imgs/ic_saudi_arabia.png"),
                                ),
                              ),
                              Text(
                                "العربية",
                                style: TextStyle(color: Colors.grey[50]),
                              )
                            ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: new CircleAvatar(
                          radius: 45.0,
                          backgroundImage:
                              AssetImage("assets/imgs/face.jpg"),
                        ),
                      ),
                    ],
                  )),
            
              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32),
                child: new ListTile(
                  title: new Text(
                    "الرئيسية",
                    style: new TextStyle(
                        fontSize: 15.0,
                        color: Colors.grey[50],
                        fontFamily: 'JF Flat'),
                  ),
                  leading: Image.asset(
                    "assets/imgs/ic_menu_home.png",
                    width: 30,
                    height: 30,
                  ),

                  onTap: () {
                    Navigator.pop(context);
                  }, //الرئيسيه
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32),
                child: new Container(
                  color: Colors.black,
                  height: 1.0,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32),
                child: new ListTile(
                  title: new Text(
                    "عن التطبيق",
                    style: new TextStyle(
                        fontSize: 15.0,
                        color: Colors.grey[50],
                        fontFamily: 'JF Flat'),
                  ),
                  leading: new Image.asset(
                    "assets/imgs/ic_menu_about.png",
                    height: 30.0,
                    width: 30.0,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32),
                child: new Container(
                  color: Colors.black,
                  height: 1.0,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32),
                child: new ListTile(
                  title: new Text(
                    "اتصل بنا",
                    style: new TextStyle(
                        fontSize: 15.0,
                        color: Colors.grey[50],
                        fontFamily: 'JF Flat'),
                  ),
                  leading: new Image.asset(
                    "assets/imgs/ic_menu_contact.png",
                    height: 30.0,
                    width: 30.0,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ),
               Padding(
                 padding: const EdgeInsets.only(left: 32 , right: 32),
                 child: new Container(
                  color: Colors.black,
                  height: 1.0,
              ),
               ),
              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32),
                child: new ListTile(
                  title: new Text(
                    "تعديل بيانات المتجر",
                    style: new TextStyle(
                        fontSize: 15.0,
                        color: Colors.grey[50],
                        fontFamily: 'JF Flat'),
                  ),
                  leading: new Image.asset(
                    "assets/imgs/ic_menu_edit_store.png",
                    height: 30.0,
                    width: 30.0,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ),
               Padding(
                 padding: const EdgeInsets.only(left: 32 , right: 32),
                 child: new Container(
                  color: Colors.black,
                  height: 1.0,
              ),
               ),
            
              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32),
                child: new ListTile(
                  title: new Text(
                    "الشروط والاحكام",
                    style: new TextStyle(
                        fontSize: 15.0,
                        color: Colors.grey[50],
                        fontFamily: 'JF Flat'),
                  ),
                  leading: new Image.asset(
                    "assets/imgs/ic_menu_privacy.png",
                    height: 30.0,
                    width: 30.0,
                  ),

                  onTap: () {
                    Navigator.pop(context);
                  }, // الاعدادات
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32),
                child: new Container(
                  color: Colors.black,
                  height: 1.0,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32),
                child: new ListTile(
                  title: new Text(
                    " الخروج من التطبيق",
                    style: new TextStyle(
                        fontSize: 15.0,
                        color: Colors.grey[50],
                        fontFamily: 'JF Flat'),
                  ),
                  leading: new Image.asset(
                    "assets/imgs/ic_menu_logout.png",
                    width: 30.0,
                    height: 30.0,
                  ),

                  onTap: () {
                    Navigator.pop(context);
                  }, //الرئيسيه
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left: 32 , right: 32 , top: 30 , bottom: 20),
                child: Column(
                 
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Image.asset(
                      "assets/imgs/ic_menu_fb.png",
                      width: 30.0,
                      height: 30.0,
                    ),

                    Image.asset(
                      "assets/imgs/ic_menu_twitter.png",
                      width: 30.0,
                      height: 30.0,
                    ),

                    Image.asset(
                      "assets/imgs/ic_menu_snap.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                      ],
                    ),

                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Image.asset(
                        "assets/imgs/ic_menu_insta.png",
                        width: 30.0,
                        height: 30.0,
                    ),

                    Image.asset(
                        "assets/imgs/ic_menu_whatsapp.png",
                        width: 30.0,
                        height: 30.0,
                    ),

                    Image.asset(
                        "assets/imgs/ic_menu_youtube.png",
                        width: 30.0,
                        height: 30.0,
                    ),
                        ],
                    ),
                      ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
