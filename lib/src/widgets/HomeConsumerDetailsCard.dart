import 'package:flutter/material.dart';
import 'package:hand_made/src/screens/family_producer/product_details_screen.dart';
import 'package:hand_made/src/screens/consumer_screens/home_consumer_details2_screen.dart';

class HomeConsumerDetails1Card extends StatefulWidget {
  @override
  _HomeConsumerDetails1CardState createState() => _HomeConsumerDetails1CardState();
}

class _HomeConsumerDetails1CardState extends State<HomeConsumerDetails1Card> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
          child: Container(
        width: MediaQuery.of(context).size.width/2.2,
        height: MediaQuery.of(context).size.height/3,
        child: Column(
          children: <Widget>[
            Container(
               width: MediaQuery.of(context).size.width/3,
        height: MediaQuery.of(context).size.height/8,
        decoration: BoxDecoration(
          shape: BoxShape.circle ,
          border: Border.all(width: 1  , color: Colors.grey[500])
        ),
        child: Image.asset("assets/imgs/ic_cat_4.png"),
            ),

           
               Text("المطبخ السورى" , style: TextStyle(fontSize: 15 , color: Colors.grey[800]),),
                Text("12 منتج" , style: TextStyle(fontSize: 12 , color: Colors.grey[500]),),
          ],
        ),
        
      ),
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
          builder: (BuildContext context)=>HomeConsumerDetails2Screen()
        ));
      },
    );
  }
}