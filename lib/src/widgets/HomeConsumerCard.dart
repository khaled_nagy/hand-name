import 'package:flutter/material.dart';
import 'package:hand_made/src/screens/consumer_screens/home_consumer_details1_screen.dart';
import 'package:hand_made/src/screens/family_producer/product_details_screen.dart';

class HomeConsumerCard extends StatefulWidget {
  @override
  _HomeConsumerCardState createState() => _HomeConsumerCardState();
}

class _HomeConsumerCardState extends State<HomeConsumerCard> {

 
  @override
  Widget build(BuildContext context) {
    return InkWell(
          child: Card(
                      child: Container(
        width: MediaQuery.of(context).size.width/2.2,
        height: MediaQuery.of(context).size.height/3,
        child: Column(
            children: <Widget>[
              Container(
                 width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height/6,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12)
        ),
        child: Image.asset("assets/imgs/ic_cat_1.png"),
              ),

             
                 Text("مطبخ الاسرة" , style: TextStyle(fontSize: 12 , color: Colors.grey[800]),),
            ],
        ),
        
      ),
          ),
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
          builder: (BuildContext context)=>HomeConsumerDetails1Screen()
        ));
      },
    );
  }
}