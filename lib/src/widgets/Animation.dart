import 'dart:math';

import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:simple_animations/simple_animations/multi_track_tween.dart';

class FadeIn extends StatelessWidget {
  final double delay;
  final Widget child;

  FadeIn({this.delay, this.child});

  @override
  Widget build(BuildContext context) {
   final tween = MultiTrackTween([
    Track("size").add(Duration(seconds: 1), Tween(begin: 0.0, end: 150.0)),
   
   
  ]);
    // TODO: implement build
    return ControlledAnimation(
      playback: Playback.MIRROR,
      duration: tween.duration,
      tween: tween,
      
    
        builder: (context, animation) {
        return  Container(
            decoration: BoxDecoration(
              color: Colors.grey[50]
            ),
            width: animation["size"],
            height: animation["size"],
           child: Image.asset("assets/imgs/192.png" ),
         
        );
      },
    );
  }
}
