import 'package:flutter/material.dart';

class NotificationCard extends StatefulWidget {
  @override
  _NotificationCardState createState() => _NotificationCardState();
}

class _NotificationCardState extends State<NotificationCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset("assets/imgs/ic_alert.png"),

              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 8 , right: 8 , ),
                        child: Directionality(
                          textDirection: TextDirection.ltr,
                          child: Text("21 May 2019" , style: TextStyle(color: Colors.grey[800]),)),
                      ),

                         Padding(
                padding: const EdgeInsets.only(left: 8 , right: 8 , ),
                child: Directionality(
                  textDirection: TextDirection.ltr,
                  child: Text("03:13AM" , style: TextStyle(color: Colors.grey[800]),)),
              ),
                    ],
                  ),

                

               Padding(
                padding: const EdgeInsets.only(left: 8 , right: 8 , ),
               
                  child: Text("تم تقيمك من قبل احمد محمد" , style: TextStyle(color: Colors.grey[500]),)
                  ,
              ),
                ],
              ),

              
            ],
          ),

        Divider()
        ],
        
      ),
    );
  }
}