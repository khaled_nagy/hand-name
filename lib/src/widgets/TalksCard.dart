import 'package:flutter/material.dart';
import 'package:hand_made/src/screens/family_producer/chat_screen.dart';

class TalksCard extends StatefulWidget {
  @override
  _TalksCardState createState() => _TalksCardState();
}

class _TalksCardState extends State<TalksCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 8,
                          right: 8,
                        ),
                        child: Directionality(
                            textDirection: TextDirection.ltr,
                            child: Text(
                              "21 May 2019",
                              style: TextStyle(color: Colors.grey[800]),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 8,
                          right: 8,
                        ),
                        child: Directionality(
                            textDirection: TextDirection.ltr,
                            child: Text(
                              "03:13AM",
                              style: TextStyle(color: Colors.grey[800]),
                            )),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 8,
                      right: 8,
                    ),
                    child: Text(
                      "محادثه مع سارة الوجدانى",
                      style: TextStyle(color: Colors.grey[500]),
                    ),
                  ),
                ],
              ),
              Image.asset("assets/imgs/ic_chat.png"),
            ],
          ),
          Divider()
        ],
      ),
    );
  }
}
