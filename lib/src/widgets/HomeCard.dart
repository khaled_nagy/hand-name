import 'package:flutter/material.dart';
import 'package:hand_made/src/screens/family_producer/product_details_screen.dart';

class HomeCard extends StatefulWidget {
  @override
  _HomeCardState createState() => _HomeCardState();
}

class _HomeCardState extends State<HomeCard> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
          child: Container(
        width: MediaQuery.of(context).size.width/2.2,
        height: MediaQuery.of(context).size.height/3,
        child: Column(
          children: <Widget>[
            Container(
               width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height/6.2,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12)
        ),
        child: Image.asset("assets/imgs/ic_cat_4.png"),
            ),

            Text("40 ريال" , style: TextStyle(fontSize: 12 , color: const Color(0xffA60A53)),),
               Text("مشغولات من الورق" , style: TextStyle(fontSize: 12 , color: Colors.grey[500]),),
          ],
        ),
        
      ),
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
          builder: (BuildContext context)=>ProductDetailsScreen()
        ));
      },
    );
  }
}