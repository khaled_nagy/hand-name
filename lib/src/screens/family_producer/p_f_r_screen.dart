import 'package:flutter/material.dart';
import 'package:hand_made/drawer.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:hand_made/src/controllers/family_producer/PFR_Controller.dart';

import 'package:hand_made/src/screens/family_producer/verificationCode.dart';

class PFR_Screen extends StatefulWidget {
  createState() => PFR_View();
}

class PFR_View extends StateMVC<PFR_Screen> {
  PFR_View() : super(PFR_Controller()) {
    _pfr_controller = PFR_Controller.con;
  }
  PFR_Controller _pfr_controller;

  String dropDownTitleCategory = "974";
  String _valueCategory;

  List<String> categories = [
    '123',
    '456',
    '789 ',
    '741',
  ];
  String sizeCategory = "";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        endDrawer: DrawerW().showDrawerUser(context),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: new DecorationImage(
                  image: new AssetImage("assets/imgs/ic_decoration.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.only(left: 32, right: 32),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height / 16,
                  ),
                  Image.asset(
                    "assets/imgs/production_famely.PNG",
                    width: MediaQuery.of(context).size.width / 3.5,
                    height: MediaQuery.of(context).size.height / 6,
                  ),
                  Text(
                    "تسجيل كاسرة منتجة",
                    style: TextStyle(fontSize: 18),
                  ),
                  new Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey[300]),
                        borderRadius: BorderRadius.circular(12)),
                    height: 50,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 0, right: 16, left: 16),
                          child: Image.asset("assets/imgs/ic_username.png", width: 20,height: 20,),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.5,
                          child: new TextField(
                            keyboardType: TextInputType.text,
                            controller: TextEditingController(),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  right: 4.0, top: 0.0, left: 4),
                              hintText: "اسم المستخدم",
                              hintStyle: new TextStyle(color: Colors.grey[400]),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    height: 16,
                  ),
                  new Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey[300]),
                        borderRadius: BorderRadius.circular(12)),
                    height: 50,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 0, right: 16, left: 16),
                          child: Image.asset("assets/imgs/ic_email.png", width: 20,height: 20,),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.5,
                          child: new TextField(
                            keyboardType: TextInputType.text,
                            controller: TextEditingController(),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  right: 4.0, top: 0.0, left: 4),
                              hintText: " البريد الالكترونى",
                              hintStyle: new TextStyle(color: Colors.grey[400]),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    height: 16,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Container(
                        decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: Colors.grey[300]),
                            borderRadius: BorderRadius.circular(12)),
                        height: 50,
                        width: MediaQuery.of(context).size.width / 2,
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 0, right: 16, left: 16),
                              child:
                                  Image.asset("assets/imgs/ic_phone.png", width: 20,height: 20,),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 3,
                              child: new TextField(
                                keyboardType: TextInputType.text,
                                controller: TextEditingController(),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  contentPadding: EdgeInsets.only(
                                      right: 4.0, top: 0.0, left: 4),
                                  hintText: "رقم الهاتف",
                                  hintStyle:
                                      new TextStyle(color: Colors.grey[400]),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        width: MediaQuery.of(context).size.width / 4.5,
                        height: 50.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(
                              width: 1, color: Colors.grey[300]),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 6),
                          child: DropdownButton<String>(
                            isDense: true,
                            icon: Icon(
                              Icons.arrow_drop_down,
                              color: const Color(0xffA60A53),
                            ),
                            hint: Padding(
                              padding: const EdgeInsets.only(right: 16, left: 8),
                              child: new Text(dropDownTitleCategory,
                                  style: new TextStyle(
                                    fontSize: 15,
                                    color: Colors.grey[600]
                                       ,
                                  )),
                            ),
                            items: categories.map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        right: 16,
                                        left: 8,
                                      ),
                                      child: new Text(
                                        value,
                                        style: TextStyle(
                                            color: const Color(0xffA60A53)),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _valueCategory = value;
                                dropDownTitleCategory = _valueCategory;
                                sizeCategory = _valueCategory;
                              });
                            },
                            value: _valueCategory,
                            isExpanded: true,
                          ),
                        ),
                      ),
                    ],
                  ),
                  new Container(
                    height: 16,
                  ),
                  new Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey[300]),
                        borderRadius: BorderRadius.circular(12)),
                    height: 50,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 0, right: 16, left: 16),
                          child: Image.asset("assets/imgs/ic_lock.png", width: 20,height: 20,),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.5,
                          child: new TextField(
                            keyboardType: TextInputType.text,
                            controller: TextEditingController(),
                            obscureText: true,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  right: 4.0, top: 0.0, left: 4),
                              hintText: "انشاء كلمة المرور",
                              hintStyle: new TextStyle(color: Colors.grey[400]),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    height: 16,
                  ),
                  new Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey[300]),
                        borderRadius: BorderRadius.circular(12)),
                    height: 50,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 0, right: 16, left: 16),
                          child: Image.asset("assets/imgs/ic_lock.png", width: 20,height: 20,),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.5,
                          child: new TextField(
                            keyboardType: TextInputType.text,
                            controller: TextEditingController(),
                            obscureText: true,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  right: 4.0, top: 0.0, left: 4),
                              hintText: " تاكيد كلمة المرور",
                              hintStyle: new TextStyle(color: Colors.grey[400]),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 16,
                  ),
                  InkWell(
                    child: new Container(
                      height: 45.0,
                      width: MediaQuery.of(context).size.width,
                      child: new Material(
                          color: const Color(0xffA60A53),
                          elevation: 0.0,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(12.0),
                          ),
                          child: new Center(
                              child: new Padding(
                                  padding: new EdgeInsets.only(
                                      top: 0.0, bottom: 0.0),
                                  child: new Text(
                                    "التالى",
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        fontFamily: 'JF Flat'),
                                  )))),
                    ),
                    onTap: () {

                      Navigator.push(context, MaterialPageRoute(
                        builder: (BuildContext context)=> VerificationCode()
                      ));
                    },
                  ),
                  Container(
                    height: 16,
                  ),
                  Text(
                    " سيرسل لك رمز التفغيل لتوثيق حسابك ",
                    style: TextStyle(fontSize: 12, color: Colors.grey[500]),
                  ),
                  Container(
                    height: 40,
                  ),
                  Text("من خلال الضغط على التالى اوكد انى قرات ووافقت على  ",
                      style: TextStyle(fontSize: 12, color: Colors.grey[500])),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      
                      Text(
                          "شروط واحكام",
                          style:
                              TextStyle(fontSize: 14, color: const Color(0xffA60A53), decoration: TextDecoration.underline)),
                      Text(
                          "خدمة نشمى بالاضافة الى",
                          style:
                              TextStyle(fontSize: 12,color: Colors.grey[500])),
                      Text(
                          "سياسة الخصوصية",
                          style:
                              TextStyle(fontSize: 14,color: const Color(0xffA60A53) , decoration: TextDecoration.underline)),
                    ],
                  )
                ],
              ),
            ),
          )),
    );
  }
}
