import 'dart:ui' as prefix0;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:hand_made/drawer.dart';

import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hand_made/src/controllers/family_producer/ProductDetailsController.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:carousel_pro/carousel_pro.dart';

class ProductDetailsScreen extends StatefulWidget {
  createState() => ProductDetailsView();
}

class ProductDetailsView extends StateMVC<ProductDetailsScreen> {
  ProductDetailsView() : super(ProductDetailsController()) {
    _productDetailsController = ProductDetailsController.con;
  }
  ProductDetailsController _productDetailsController;

  int photoIndex = 0 ;

  List<String> photos =[
    "aasets/imgs/ads1.png",
    "aasets/imgs/ads2.png",
    "aasets/imgs/ads3.png",
    
  ];

  void _previousImage(){
    setState((){
      photoIndex = photoIndex > 0 ? photoIndex  -1 : 0 ;
    });
  }

    void _nextImage(){
    setState((){
      photoIndex = photoIndex < photos.length -1 ? photoIndex  + 1 : photoIndex ;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        endDrawer: DrawerW().showDrawerUser(context),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
           
              children: <Widget>[
                Image.asset("assets/imgs/ic_edit.png",width: 30,height: 30,),

                 Padding(
                   padding: const EdgeInsets.only(left: 50 , right: 50),
                   child: InkWell(
                     child: Image.asset("assets/imgs/ic_trash.png",width: 25,height: 25,),
                     onTap: (){
                       _productDetailsController.openBottomSheetDelete(context);
                     },),
                 ),
              ],
            ),

           
Text("100 ريال")

          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "المحادثات",
          style: TextStyle(color: Colors.grey[800]),
        ),
        backgroundColor: Colors.grey[50],
        elevation: 0,
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: new DecorationImage(
                  image: new AssetImage("assets/imgs/ic_decoration.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.only(left: 32, right: 32),
            child: Column(
              children: <Widget>[
                Container(
  height: 200.0,
  width: 300.0,
  decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(12)
  ),
  child: new Carousel(
    images: [
      new ExactAssetImage('assets/imgs/ads1.png'),
      new ExactAssetImage('assets/imgs/ads2.png'),
      new ExactAssetImage("assets/imgs/ads3.png"),
    ],
  )
),

                 
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "شغل يدوى بالكروشية رائع",
                    style: TextStyle(color: Colors.grey[800], fontSize: 18),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "اسم المتجر",
                        style: TextStyle(color: const Color(0xffA60A53), fontSize: 18),
                      ),
                    ],
                  ),
                ),

                
                    Text(
                "حيث ننتقل من تعريفكم بالفن الى ارساله لمنازلكم مباشرة ! هذا المتجر خاص بموقع وقناة الاعمال اليدوية للجميع .",
                style: TextStyle(color: Colors.grey[500], fontSize: 15),
              ),
                 
              ],
            ),
          )),
    );
  }
}
