import 'package:hand_made/drawer.dart';
import 'package:hand_made/src/screens/family_producer/verificationCode.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter/material.dart';

import 'package:hand_made/src/controllers/family_producer/ActivityDataController.dart';

import 'package:hand_made/focushelpwidget.dart';

import 'package:hand_made/bottom_navigation.dart';
import 'package:hand_made/src/screens/consumer_screens/bottom_navigation_bar_consumer_screen.dart';

class ActivityDataScreen extends StatefulWidget {
  createState() => ActivityDataView();
}

class ActivityDataView extends StateMVC<ActivityDataScreen> {
  ActivityDataView() : super(ActivityDataController()) {
    _activityDataController = ActivityDataController.con;
  }

  ActivityDataController _activityDataController;

  String dropDownTitleCategory = "969";
  String _valueCategory;

  List<String> categories = [
    '123',
    '456',
    '789 ',
    '741',
  ];
  String sizeCategory = "";

  FocusNode _descfocus = new FocusNode();
  FocusNode _descfocus1 = new FocusNode();
  TextEditingController _JopDescriptionContoller = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        endDrawer: DrawerW().showDrawerUser(context),
      appBar: AppBar(
        leading: InkWell(
          child: Icon(
            Icons.arrow_back,
            color: Colors.grey[800],
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "اضف بيانات النشاط",
          style: TextStyle(color: Colors.grey[800]),
        ),
        centerTitle: true,
        backgroundColor: Colors.grey[50],
        elevation: 0,
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.only(left: 32, right: 32),
          child: SingleChildScrollView(
                      child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: CircleAvatar(
                    radius: 40,
                    backgroundImage:
                        AssetImage("assets/imgs/ic_upload_avatar.png"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 16),
                  child: Text(
                    "شعار النشاط",
                    style: TextStyle(
                      color: Colors.grey[500],
                      fontSize: 18,
                    ),
                  ),
                ),
                new Container(
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey[300]),
                      borderRadius: BorderRadius.circular(12)),
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 0, right: 16, left: 16),
                        child: Image.asset("assets/imgs/ic_username.png", width: 20,height: 20,),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.5,
                        child: new TextField(
                          keyboardType: TextInputType.text,
                          controller: TextEditingController(),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding:
                                EdgeInsets.only(right: 4.0, top: 0.0, left: 4),
                            hintText: "اسم النشاط",
                            hintStyle: new TextStyle(color: Colors.grey[400]),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Container(
                        decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.grey[300]),
                            borderRadius: BorderRadius.circular(12)),
                        height: 50,
                        width: MediaQuery.of(context).size.width / 2.5,
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 0, right: 16, left: 16),
                              child: Image.asset("assets/imgs/ic_phone.png",width: 20,height: 20,),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 4,
                              child: new TextField(
                                keyboardType: TextInputType.text,
                                controller: TextEditingController(),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  contentPadding: EdgeInsets.only(
                                      right: 4.0, top: 0.0, left: 4),
                                  hintText: "المدينة",
                                  hintStyle:
                                      new TextStyle(color: Colors.grey[400]),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        width: MediaQuery.of(context).size.width / 2.5,
                        height: 50.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(width: 1, color: Colors.grey[300]),
                        ),
                        child: Padding(
                            padding: const EdgeInsets.only(top: 6),
                            child: GestureDetector(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Image.asset("assets/imgs/ic_email.png" , width: 20,height: 20,),
                                  Text("القسم"),
                                  Image.asset("assets/imgs/ic_arrow_down.png")
                                ],
                              ),
                              onTap: () {
                                _activityDataController
                                    .openBottomSheetDepartment(context);
                              },
                            )),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 16, bottom: 4, left: 8, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("وصف النشاط",
                          style: TextStyle(color: Colors.grey[800])),
                      Text("200 كلمه", style: TextStyle(color: Colors.grey[500])),
                    ],
                  ),
                ),
                new Container(
                  height: 88.0,
                  child: new Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0)),
                    color: Colors.grey[50],
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          // margin: new EdgeInsets.all(16.0),
                          width: 360.0,
                          height: 80.0,
                          decoration: new BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                              shape: BoxShape.rectangle,
                              border:
                                  Border.all(width: 1, color: Colors.grey[300])),
                          child: new EnsureVisibleWhenFocused(
                              focusNode: _descfocus,
                              child: TextFormField(
                                focusNode: _descfocus,
                                style: new TextStyle(color: Colors.grey[800]),
                                obscureText: false,
                                controller: _JopDescriptionContoller,
                                maxLength: 700,
                                maxLines: 8,
                                decoration: new InputDecoration.collapsed(
                                    hintText: "وصف النشاط",
                                    hintStyle: new TextStyle(
                                        color: Colors.grey[500],
                                        fontFamily: 'JF Flat')),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16, bottom: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Container(
                        decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.grey[300]),
                            borderRadius: BorderRadius.circular(12)),
                        height: 50,
                        width: MediaQuery.of(context).size.width / 2,
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 0, right: 16, left: 16),
                              child: Image.asset("assets/imgs/ic_phone_fill.png"),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 3,
                              child: new TextField(
                                keyboardType: TextInputType.text,
                                controller: TextEditingController(),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  contentPadding: EdgeInsets.only(
                                      right: 4.0, top: 0.0, left: 4),
                                  hintText: "رقم الهاتف",
                                  hintStyle:
                                      new TextStyle(color: Colors.grey[400]),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        width: MediaQuery.of(context).size.width / 4.5,
                        height: 50.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(width: 1, color: Colors.grey[300]),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 6),
                          child: DropdownButton<String>(
                            isDense: true,
                            icon: Icon(
                              Icons.arrow_drop_down,
                              color: const Color(0xffA60A53),
                            ),
                            hint: Padding(
                              padding: const EdgeInsets.only(right: 16, left: 8),
                              child: new Text(dropDownTitleCategory,
                                  style: new TextStyle(
                                    fontSize: 15,
                                    color: Colors.grey[600],
                                  )),
                            ),
                            items: categories.map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        right: 16,
                                        left: 8,
                                      ),
                                      child: new Text(
                                        value,
                                        style: TextStyle(
                                            color: const Color(0xffA60A53)),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _valueCategory = value;
                                dropDownTitleCategory = _valueCategory;
                                sizeCategory = _valueCategory;
                              });
                            },
                            value: _valueCategory,
                            isExpanded: true,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                InkWell(
                  child: new Container(
                    height: 45.0,
                    width: MediaQuery.of(context).size.width,
                    child: new Material(
                        color: const Color(0xffA60A53).withOpacity(0.2),
                        elevation: 0.0,
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(12.0),
                        ),
                        child: new Center(
                            child: new Padding(
                                padding:
                                    new EdgeInsets.only(top: 0.0, bottom: 0.0),
                                child: new Text(
                                  "اتمام التسجيل",
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontFamily: 'JF Flat'),
                                )))),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                BottomNavigationBarConsumer()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
