import 'package:flutter/material.dart';
import 'package:hand_made/drawer.dart';
import 'package:hand_made/src/controllers/family_producer/Login1Controller.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hand_made/src/screens/family_producer/login2_screen.dart';
import 'package:hand_made/src/screens/family_producer/p_f_r_screen.dart';
import 'package:hand_made/src/screens/consumer_screens/p_f_r_screen.dart';

class Login1Screen extends StatefulWidget {
  createState() => Login1View();
}

class Login1View extends StateMVC<Login1Screen> {
  Login1View() : super(Login1Controller()) {
    _login1controller = Login1Controller.con;
  }
  Login1Controller _login1controller;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        endDrawer: DrawerW().showDrawerUser(context),
      body: Container(
        
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: new DecorationImage(
                image: new AssetImage("assets/imgs/ic_decoration.png"),
                 fit: BoxFit.cover)
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 32, right: 32),
          child: SingleChildScrollView(
                      child: Column(
              children: <Widget>[
                Container(
                 
                  height: MediaQuery.of(context).size.height / 16,
                ),
                Image.asset(
                  "assets/imgs/production_famely.PNG",
                  width: MediaQuery.of(context).size.width / 2.5,
                  height: MediaQuery.of(context).size.height / 4,
                ),
                Container(
                  
                  height: MediaQuery.of(context).size.height / 30,
                ),
                InkWell(
                                child: Container(
                    height: MediaQuery.of(context).size.height / 5,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      border: Border.all(color: Color(0xff8C0343), width: 2),
                      color: Colors.grey[50],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Image.asset(
                            "assets/imgs/ic_productive_family.png",
                            width: MediaQuery.of(context).size.width / 4,
                            height: MediaQuery.of(context).size.height / 9,
                          ),
                        ),
                        Container(
                          height: 4,
                        ),
                        Text(
                          "اسرة منتجة",
                          style: TextStyle(fontSize: 18),
                        )
                      ],
                    ),
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context)=> Login2Screen()
                    ));
                  },
                ),
                 Container(
                        height: 8,
                      ),
                InkWell(
                                child: Container(
                    height: 50,
                    child: Text(
                      "ليس لديك حساب اسرة منتجة ؟",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  onTap: (){
Navigator.push(context, MaterialPageRoute(
  builder: (BuildContext context)=> PFR_Screen()
));
                  },
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 20,
                ),

                 InkWell(
                                  child: Container(
                    height: MediaQuery.of(context).size.height / 5,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      border: Border.all(color: Color(0xff8C0343), width: 2),
                      color: Colors.grey[50],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Image.asset(
                            "assets/imgs/ic_consumer.png",
                            width: MediaQuery.of(context).size.width / 4,
                            height: MediaQuery.of(context).size.height / 9,
                          ),
                        ),
                       
                        Text(
                          "مستهلك",
                          style: TextStyle(fontSize: 18),
                        )
                      ],
                    ),
                ),onTap: (){
                  Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context)=> Login2Screen()
                    ));
                },
                 ),
                Container(
                        height: 8,
                      ),
                InkWell(
                                child: Container(
                    height: 50,
                    child: Text(
                      "ليس لديك حساب مستهلك ؟",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  onTap: (){
Navigator.push(context, MaterialPageRoute(
  builder: (BuildContext context)=> PFR_ConsumerScreen()
));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
