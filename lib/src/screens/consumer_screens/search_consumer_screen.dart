import 'package:flutter/material.dart';
import 'package:hand_made/drawer.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:hand_made/src/controllers/consumer_controller/SearchConsumerController.dart';

class SearchConsumerScreen extends StatefulWidget {
  createState() => SearchConsumerView();
}

class SearchConsumerView extends StateMVC<SearchConsumerScreen> {
  SearchConsumerView() : super() {
    _searchConsumerController = SearchConsumerController.con;
  }
  SearchConsumerController _searchConsumerController;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      endDrawer: DrawerW().showDrawerUser(context),
      appBar: AppBar(
        centerTitle: true,
        title: Text("بحث" , style: TextStyle(color: Colors.grey[800]),),
        backgroundColor: Colors.grey[50],
        elevation: 0,


      ),
      
      body: Padding(
        padding: const EdgeInsets.fromLTRB(32, 20, 32, 0),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.grey[200],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.search,
                      size: 30,
                      color: Colors.grey[800],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/1.9,

                    child: TextField(
                     
                      keyboardType: TextInputType.text,
                      controller: TextEditingController(),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding:
                            EdgeInsets.only(right: 4.0, top: 0.0, left: 4),
                        hintText: "بحث",
                        hintStyle: new TextStyle(color: Colors.grey[800]),
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.mic,
                      size: 30,
                      color: Colors.grey[800],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
