import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:hand_made/drawer.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:hand_made/src/controllers/consumer_controller/HomeConsumerDetails2Controller.dart';
import 'package:hand_made/src/widgets/HomeConsumerDetails2Card.dart';

class HomeConsumerDetails2Screen extends StatefulWidget {
  createState() => HomeConsumerDetails2View();
}

class HomeConsumerDetails2View extends StateMVC<HomeConsumerDetails2Screen> {
  HomeConsumerDetails2View() : super(HomeConsumerDetails2Controller()) {
    _homeConsumerDetails2Controller = HomeConsumerDetails2Controller.con;
  }

  HomeConsumerDetails2Controller _homeConsumerDetails2Controller;

   double seleListRatingHearts = 3.5;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   
        return Scaffold(
          endDrawer: DrawerW().showDrawerUser(context),
          body: Padding(
            padding: const EdgeInsets.only(left: 16, right: 16, top: 20),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        size: 30,
                        color: Colors.grey[800],
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.call,
                        size: 30,
                        color: Colors.grey[800],
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
                Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width / 3,
                    height: MediaQuery.of(context).size.height / 8,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(width: 1, color: Colors.grey[500])),
                    child: Image.asset("assets/imgs/ic_cat_4.png"),
                  ),
                ),
    
                Text("المطبخ السورى"  , style: TextStyle(color: Colors.grey[800] , fontSize: 18),),
    
                new StarRating(
                    rating: seleListRatingHearts,
                size: 18,
                color: const Color(0xffA60A53),
                borderColor: Colors.grey,
                starCount: 5,
                onRatingChanged: (_rating) => setState(
                      () {
                        this.seleListRatingHearts = _rating;
                      },
                    ),
              ),

              Text(
                "حيث ننتقل من تعريفكم بالفن الى ارساله لمنازلكم مباشرة ! هذا المتجر خاص بموقع وقناة الاعمال اليدوية للجميع .",
                style: TextStyle(color: Colors.grey[500], fontSize: 15),
              ),

               Expanded(
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  physics: AlwaysScrollableScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: 6,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return HomeConsumerDetails2Card();
                  },
                ),
              )
          ],
        ),
      ),
    );
  }
}
