
import 'package:flutter/material.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:hand_made/src/screens/splash_screen.dart';

class MVCApp extends AppMVC {
  static MaterialApp _app;

  final appTheme = ThemeData(
    brightness: Brightness.light,
    fontFamily: 'Dubai',
    // primaryColorDark: const Color(0xff000000),
    // primaryColorLight: const Color(0xff000000),
    // primaryColor: const Color(0xff000000),
    // accentColor: const Color(0xff000000),
    // dividerColor: const Color(0xffBDBDBD),
  );

  Widget build(BuildContext context) {
    _app = MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'hand_made app',
        theme: appTheme,
        home: SplashScreen(),
        routes: <String, WidgetBuilder>{
          // "/Splash": (context) => SplashScreen(),
        
        },
        builder: (BuildContext context, Widget child) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: Builder(
              builder: (BuildContext context) {
                return MediaQuery(
                    data: MediaQuery.of(context).copyWith(
                      textScaleFactor: 1.0,
                    ),
                    child: child);
              },
            ),
          );
        });
    return _app;
  }
}
