import 'package:mvc_pattern/mvc_pattern.dart';

class HomeConsumerController extends ControllerMVC{


  factory HomeConsumerController() {
    if (_this == null) _this = HomeConsumerController._();
    return _this;
  }
  static HomeConsumerController _this;

  HomeConsumerController._();

  static HomeConsumerController get con => _this;
}