import 'package:mvc_pattern/mvc_pattern.dart';

class Login2Controller extends ControllerMVC{


  factory Login2Controller() {
    if (_this == null) _this = Login2Controller._();
    return _this;
  }
  static Login2Controller _this;

  Login2Controller._();

  static Login2Controller get con => _this;
}