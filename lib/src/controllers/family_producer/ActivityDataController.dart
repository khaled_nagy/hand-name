import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter/material.dart';

class ActivityDataController extends ControllerMVC{


    factory ActivityDataController() {
    if (_this == null) _this = ActivityDataController._();
    return _this;
  }
  static ActivityDataController _this;

  ActivityDataController._();

  static ActivityDataController get con => _this;

  bool _value1 = true ;
  bool _value2 = false ;
  bool _value3 = true ;
  bool _value4 = false ;

Widget openBottomSheetDepartment(BuildContext context){

showModalBottomSheet(
  context: context,
  builder: (BuildContext context){
    return ListView(
      children: <Widget>[

        ListTile(
          contentPadding: EdgeInsets.all(16),
          title: Text("مطبخ الاسرة", style: TextStyle(fontSize: 18 , color: Colors.grey[800]) ),
          leading: Checkbox(
            
            checkColor: const Color(0xffA60A53),
            value: _value1,
          ),
        ),

         ListTile(
           contentPadding: EdgeInsets.all(16),
          title: Text("صالون الاسرة", style: TextStyle(fontSize: 18, color: Colors.grey[800])),
          leading: Checkbox(
            activeColor: const Color(0xffA60A53),
            checkColor: const Color(0xffA60A53),
            value: _value2,
          ),
        ),

         ListTile(
           contentPadding: EdgeInsets.all(16),
          title: Text(" مشغولات يدوية", style: TextStyle(fontSize: 18, color: Colors.grey[800])),
          leading: Checkbox(
            activeColor: const Color(0xffA60A53),
            checkColor: const Color(0xffA60A53),
            value: _value3,
          ),
        ),

         ListTile(
           contentPadding: EdgeInsets.all(16),
          title: Text("ضيافة الاسرة"  , style: TextStyle(fontSize: 18, color: Colors.grey[800]),),
          leading: Checkbox(
            activeColor: const Color(0xffA60A53),
            checkColor: const Color(0xffA60A53),
            value: _value4,
          ),
        ),

      ],
    );
  }
);



}

}