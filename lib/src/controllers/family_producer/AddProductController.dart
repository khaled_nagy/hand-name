import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class AddProductController extends ControllerMVC {
  factory AddProductController() {
    if (_this == null) _this = AddProductController._();
    return _this;
  }
  static AddProductController _this;

  AddProductController._();

  static AddProductController get con => _this;
  bool _value1 = true;
  bool _value2 = false;
  bool _value3 = true;
  bool _value4 = false;

  Widget openBottomSheetDepartment(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return ListView(
            children: <Widget>[
              ListTile(
                contentPadding: EdgeInsets.all(16),
                title: Text("مطبخ الاسرة",
                    style: TextStyle(fontSize: 18, color: Colors.grey[800])),
                leading: Checkbox(
                  checkColor: const Color(0xffA60A53),
                  value: _value1,
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.all(16),
                title: Text("صالون الاسرة",
                    style: TextStyle(fontSize: 18, color: Colors.grey[800])),
                leading: Checkbox(
                  activeColor: const Color(0xffA60A53),
                  checkColor: const Color(0xffA60A53),
                  value: _value2,
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.all(16),
                title: Text(" مشغولات يدوية",
                    style: TextStyle(fontSize: 18, color: Colors.grey[800])),
                leading: Checkbox(
                  activeColor: const Color(0xffA60A53),
                  checkColor: const Color(0xffA60A53),
                  value: _value3,
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.all(16),
                title: Text(
                  "ضيافة الاسرة",
                  style: TextStyle(fontSize: 18, color: Colors.grey[800]),
                ),
                leading: Checkbox(
                  activeColor: const Color(0xffA60A53),
                  checkColor: const Color(0xffA60A53),
                  value: _value4,
                ),
              ),
            ],
          );
        });
  }

  Widget openBottomSheetDelevrytime(BuildContext context){
    showModalBottomSheet(
      context: context ,
      builder: (BuildContext context){
        return Padding(
          padding: const EdgeInsets.only(left: 32 , right: 32),
          child: ListView(
              children: <Widget>[
                ListTile(
                  contentPadding: EdgeInsets.all(8),
                  title: Text("تسليم فورى",
                      style: TextStyle(fontSize: 18, color: Colors.grey[800])),
                  
                ),

                Divider(),
                ListTile(
                  contentPadding: EdgeInsets.all(8),
                  title: Text("ساعة الى ساعتين",
                      style: TextStyle(fontSize: 18, color: Colors.grey[800])),
                 
                ),
                ListTile(
                  contentPadding: EdgeInsets.all(8),
                  title: Text("ساعتين الى اربع ساعات",
                      style: TextStyle(fontSize: 18, color: Colors.grey[800])),
                
                ),
                ListTile(
                  contentPadding: EdgeInsets.all(8),
                  title: Text(
                    "يوم",
                    style: TextStyle(fontSize: 18, color: Colors.grey[800]),
                  ),
                 
                ),

                  ListTile(
                  contentPadding: EdgeInsets.all(8),
                  title: Text(
                    "يومان",
                    style: TextStyle(fontSize: 18, color: Colors.grey[800]),
                  ),
                 
                ),
              ],
            ),
        );
      }
      );
  }

    Widget openBottomSheetDelevry(BuildContext context){
    showModalBottomSheet(
      context: context ,
      builder: (BuildContext context){
        return Padding(
          padding: const EdgeInsets.only(left: 32 , right: 32),
          child: ListView(
              children: <Widget>[
                ListTile(
                  contentPadding: EdgeInsets.all(8),
                  title: Text("لا يوجد توصيل",
                      style: TextStyle(fontSize: 18, color: Colors.grey[800])),
                  
                ),

                Divider(),
                ListTile(
                  contentPadding: EdgeInsets.all(8),
                  title: Text("التوصيل مجانا",
                      style: TextStyle(fontSize: 18, color: Colors.grey[800])),
                 
                ),
                 Divider(),
                ListTile(
                  contentPadding: EdgeInsets.all(8),
                  title: Text("توصيل باجر",
                      style: TextStyle(fontSize: 18, color: Colors.grey[800])),
                
                ),
               
              ],
            ),
        );
      }
      );
  }
}
